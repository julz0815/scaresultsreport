# Veracaode - Gitlab SCA results report and issue generation 

## About

this little Java Script will rewrite Veracode's Agent Based SCA json results in Gitlab readable report format in order display results as dependency scanning on the pipeline run
 
## Instructions

- Copy the dependencies.js file into your rpository
- set up a job to run Veracode Agent Based SCA
- install node.js, npm, axios and Mathjs on the image you are using, using `before_script`
- run Veracode Agent Based SCA using `script`
- run the nodejs dependencies.js script using `after_script`


## Command arguments issue creation
If you only want to display results on the security tab of your pipeline you only run the script.  
If you want to also create issues from findings there are 4 command arguments you need to set  
Duplication checks are done for the last 100 exisiting issues for now. If the issue is not part of these 100 exisitng issues, it will be recreated.  
1st - GitLab Private token  
2nd - can be set to true or false - it will enable the issue generation  
3rd - needs to hold your Gitlab project ID on which you want to create the issues  
4th - can be true or false and allows you to create a new issue if there is already a closed issue, but the findings is still showing up on a newer scan  
5th - ONLY FOR DOCKER SCANNING - the image name to scan  
  
an example call for this Gitlab project would be `nodejs ./dependencies_[app|container].js ${PRIVATE_TOKEN} true 24614889 true`


## Security report creation

The Java Script will take the input file `scaResults.json` that is required to be generated with the agent flag `--json scaResults.json`.
As well the output of the Agent Based SCA scan and the output of the Java Script (`output-sca-vulnerabilites.json`) are required to be stored as artifacts. The Java Script output is the actual report read by Gitlab and need to be set as report artifact as well.  
 
Correspondingly if you want to generate a report for a container scan the input file needs to be `scaResults.json` and the Java Script will generate the report at `output-sca-vulnerabilites-container.json`

The below example is using the `maven:3.6.0-jdk-8` as my demo application relies on maven as package manager that is required to correctly run Veracode#s Agent Based SCA. Any other image could be used to reflect the package manager of your application. Only make sure node is installed on that image.

## Example job SCA on an application


```yml
Software Composition Analysis App 3rd party:
    image: maven:3.6.0-jdk-8
    stage: Security_Scan
    before_script:
        - curl -sL https://deb.nodesource.com/setup_17.x | bash -
        - apt-get update && apt-get -y install nodejs
        - npm install axios
        - npm install mathjs
    script:
        - curl -sSL https://download.sourceclear.com/ci.sh | bash -s scan . --update-advisor --json scaResults.json
    after_script:
        - nodejs ./dependencies_app.js ${PRIVATE_TOKEN} true 24614889 false
    artifacts:
        reports:
            dependency_scanning: output-sca-vulnerabilites.json
        paths:
            - sca_output.txt
            - output-sca-vulnerabilites.json
        when: always
        name: "veracode-SCA-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
    allow_failure: false
```
  
## Example job SCA on a docker container

```yml
Software Composition Analysis App 3rd party:
    image: maven:3.6.0-jdk-8
    stage: Security_Scan
    before_script:
        - curl -sL https://deb.nodesource.com/setup_17.x | bash -
        - apt-get update && apt-get -y install nodejs
        - npm install axios
        - npm install mathjs
    script:
        - curl -sSL https://download.sourceclear.com/ci.sh | bash -s scan --image juliantotzek/verademo1-tomcat --json scaResultsContainer.json --allow-dirty
    after_script:
        - nodejs ./dependencies_container.js ${PRIVATE_TOKEN} true 24614889 false juliantotzek/verademo1-tomcat
    artifacts:
        reports:
            container_scanning: output-sca-vulnerabilites-container.json
        paths:
            - sca_output.txt
            - output-sca-vulnerabilites.json
        when: always
        name: "veracode-SCA-$CI_COMMIT_REF_NAME-$CI_COMMIT_SHORT_SHA"
    allow_failure: false
```  
  
## Example output on your pipeline security tab
![](Screenshot.png)
![](container-scanning-output.png)

## Example output on your issue list
Issues list
![](Issues1.png)

Issues details
![](Issues2.png)



